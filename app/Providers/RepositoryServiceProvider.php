<?php

namespace App\Providers;

use App\Repositories\Interfaces\iTaskRepository;
use App\Repositories\Interfaces\iTaskListRepository;
use App\Repositories\Interfaces\iUserRepository;
use App\Repositories\TaskRepository;
use App\Repositories\TaskListRepository;
use App\Repositories\UsersRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            iUserRepository::class,
            UsersRepository::class
        );
        $this->app->bind(
            iTaskListRepository::class,
            TaskListRepository::class
        );
        $this->app->bind(
            iTaskRepository::class,
            TaskRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
