<?php

namespace App\Providers;

use App\Services\Interfaces\iTaskService;
use App\Services\Interfaces\iTaskListService;
use App\Services\Interfaces\iUserService;
use App\Services\TaskService;
use App\Services\TaskListService;
use App\Services\UsersService;
use Illuminate\Support\ServiceProvider;

class ServicesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            iUserService::class,
            UsersService::class
        );
        $this->app->bind(
            iTaskListService::class,
            TaskListService::class
        );
        $this->app->bind(
            iTaskService::class,
            TaskService::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
