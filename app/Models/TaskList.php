<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaskList extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'user_id',
    ];

    public function items()
    {
        return $this->hasMany(Task::class, 'list_id', 'id');
    }

    protected $table = 'task_list';
}
