<?php


namespace App\Repositories\Interfaces;


use Illuminate\Database\Eloquent\Model;

interface CanGetOne
{
    public function getById(int $id, $columns): Model;
}
