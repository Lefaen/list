<?php


namespace App\Repositories\Interfaces;


interface iUserRepository extends CanGetOne, CanGetAll, CanGetAllWithPaginate
{

}
