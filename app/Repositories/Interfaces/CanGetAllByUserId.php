<?php


namespace App\Repositories\Interfaces;


use Illuminate\Database\Eloquent\Collection;

interface CanGetAllByUserId
{
    public function getAllByUserId($id): Collection;
}
