<?php


namespace App\Repositories\Interfaces;


use Illuminate\Database\Eloquent\Model;

interface CanGetRandom
{
    public function getRandom(): Model;
}
