<?php


namespace App\Repositories\Interfaces;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

interface CanGetAllWithPaginate
{
    public function getAllWithPaginate(int $perPage): LengthAwarePaginator;
}
