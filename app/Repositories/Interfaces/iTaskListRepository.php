<?php


namespace App\Repositories\Interfaces;


interface iTaskListRepository extends CanGetOne, CanGetAll, CanGetAllByUserId, CanGetAllWithPaginate
{

}
