<?php


namespace App\Repositories\Interfaces;


use Illuminate\Database\Eloquent\Collection;

interface CanGetAllById
{
    public function getAllById($id): Collection;
}
