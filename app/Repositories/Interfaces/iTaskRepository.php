<?php


namespace App\Repositories\Interfaces;


interface iTaskRepository extends CanGetOne, CanGetAll, CanGetAllById, CanGetAllActive, CanGetAllWithPaginate
{

}
