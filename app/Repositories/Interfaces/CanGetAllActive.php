<?php


namespace App\Repositories\Interfaces;


use Illuminate\Database\Eloquent\Collection;

interface CanGetAllActive
{
    public function getAllActive(): Collection;
}
