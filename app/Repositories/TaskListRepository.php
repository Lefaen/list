<?php


namespace App\Repositories;


use App\Models\TaskList;
use App\Repositories\Interfaces\iTaskListRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class TaskListRepository implements iTaskListRepository
{
    public function getById(int $id, $columns = ['*']): TaskList
    {
        $list = TaskList::find($id, $columns);
        return $list;
    }

    public function getAll(): Collection
    {
        $lists = TaskList::all();
        return $lists;
    }

    public function getAllWithPaginate(int $perPage = 10): LengthAwarePaginator
    {
        $lists = TaskList::orderBy('id', 'desc')->paginate($perPage);
        return $lists;
    }

    public function getAllByUserId($id): Collection
    {
        $lists = TaskList::all()->where('user_id', $id);
        return $lists;
    }
}
