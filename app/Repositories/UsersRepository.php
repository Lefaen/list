<?php


namespace App\Repositories;


use App\Models\User;
use App\Repositories\Interfaces\iUserRepository;
use \Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class UsersRepository implements iUserRepository
{
    public function getById(int $id, $columns = ['*']): User
    {
        $user = User::find($id, $columns);
        return $user;
    }

    public function getRandom(): User
    {
        $user =  User::all()->random(1)->first();
        return $user;
    }

    public function getAll(): Collection
    {
        $users = User::all();
        return $users;
    }

    public function getAllWithPaginate(int $perPage = 10): LengthAwarePaginator
    {
        $users = User::orderBy('id', 'desc')->paginate($perPage);
        return $users;
    }
}
