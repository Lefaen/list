<?php


namespace App\Repositories;


use App\Models\Task;
use App\Repositories\Interfaces\iTaskRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class TaskRepository implements iTaskRepository
{
    public function getById(int $id, $columns = ['*']): Task
    {
        $item = Task::find($id, $columns);
        return $item;
    }

    public function getAll(): Collection
    {
        $items = Task::all();
        return $items;
    }

    public function getAllWithPaginate(int $perPage = 10): LengthAwarePaginator
    {
        $items = Task::orderBy('id', 'desc')->paginate($perPage);
        return $items;
    }

    public function getAllById($id): Collection
    {
        $items = Task::all()->where('list_id', $id);
        return $items;
    }

    public function getAllActive(): Collection
    {
        $items = Task::all()->where('is_public', true);
        return $items;
    }
}
