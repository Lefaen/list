<?php


namespace App\Services;


use App\DataTransferObjects\DataTransferObject;
use App\DataTransferObjects\UserDto;
use App\Models\User;
use App\Repositories\Interfaces\iUserRepository;
use App\Repositories\UsersRepository;
use App\Services\Interfaces\iUserService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Str;


class UsersService implements iUserService
{
    private iUserRepository $repository;

    public function __construct(iUserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(DataTransferObject $dto): void
    {
        $dto->generateToken();
        User::create($dto->toArray());
    }

    public function update(int $id, DataTransferObject $dto): void
    {
        User::find($id)->update($dto->toArray());
    }

    public function getById(int $id, $columns = null): User
    {
        $columns = [
            'id',
            'name',
            'email',
            'created_at',
            'updated_at',
        ];
        $user = $this->repository->getById($id, $columns);
        return $user;
    }

    public function deleteById(int $id): void
    {
        $user = User::find($id);
        $user->delete();
    }

    public function getAll(): Collection
    {
        $users = $this->repository->getAll();
        return $users;
    }

    public function getAllWithPaginate(int $perPage = 10): LengthAwarePaginator
    {
        $users = $this->repository->getAllWithPaginate($perPage);
        return $users;
    }
}
