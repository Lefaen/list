<?php


namespace App\Services;


use App\DataTransferObjects\DataTransferObject;
use App\DataTransferObjects\TaskListDto;
use App\Models\TaskList;
use App\Repositories\Interfaces\iTaskListRepository;
use App\Services\Interfaces\iTaskListService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class TaskListService implements iTaskListService
{
    private iTaskListRepository $repository;

    public function __construct(iTaskListRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(DataTransferObject $dto): void
    {
        TaskList::create($dto->toArray());
    }

    public function deleteById(int $id): void
    {
        $list = TaskList::find($id);
        $list->delete();
    }

    public function getAll(): Collection
    {
        $lists = $this->repository->getAll();
        return $lists;
    }

    public function getAllWithPaginate(int $perPage = 10): LengthAwarePaginator
    {
        $lists = $this->repository->getAllWithPaginate($perPage);
        return $lists;
    }

    public function getById(int $id): TaskList
    {
        $list = $this->repository->getById($id, ['*']);
        return $list;
    }

    public function update(int $id, DataTransferObject $dto): void
    {
        TaskList::find($id)
            ->update($dto->toArray());
    }

    public function getAllByUserId($id): Collection
    {
        $lists = $this->repository->getAllByUserId($id);
        return $lists;
    }
}
