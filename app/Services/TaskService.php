<?php


namespace App\Services;


use App\DataTransferObjects\DataTransferObject;
use App\Models\Task;
use App\Repositories\Interfaces\iTaskRepository;
use App\Services\Interfaces\iTaskService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class TaskService implements iTaskService
{
    private iTaskRepository $repository;

    public function __construct(iTaskRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAll(): Collection
    {
        $this->repository->getAll();
    }

    public function getAllById($id): Collection
    {
        $items = $this->repository->getAllById($id);
        return $items;
    }

    public function getAllWithPaginate(int $perPage = 10): LengthAwarePaginator
    {
        return $this->repository->getAllWithPaginate($perPage);
    }

    public function getAllActive(): Collection
    {
        $items = $this->repository->getAllActive();
        return $items;
    }

    public function getById(int $id): Task
    {
        return $this->repository->getById($id, ['*']);
        return $item;
    }

    public function create(DataTransferObject $dto): void
    {
        Task::create($dto->toArray());
    }

    public function update(int $id, DataTransferObject $dto): void
    {
       Task::find($id)->update($dto->toArray());
    }

    public function deleteById(int $id): void
    {
        $item = Task::find($id);
        $item->delete();
    }

    public function deleteByListId(int $id): void
    {
        Task::where('list_id', $id)->delete();
    }
}
