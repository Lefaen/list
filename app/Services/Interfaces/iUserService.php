<?php


namespace App\Services\Interfaces;


interface iUserService extends
    CanCreate,
    CanUpdate,
    CanGetAll,
    CanGetAllWithPaginate,
    CanGetOne,
    CanDelete
{

}
