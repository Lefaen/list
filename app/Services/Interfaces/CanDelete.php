<?php


namespace App\Services\Interfaces;


interface CanDelete
{
    public function deleteById(int $id): void;
}
