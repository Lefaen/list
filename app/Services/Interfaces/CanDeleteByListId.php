<?php


namespace App\Services\Interfaces;


interface CanDeleteByListId
{
    public function deleteByListId(int $id): void;
}
