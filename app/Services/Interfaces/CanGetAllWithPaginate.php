<?php


namespace App\Services\Interfaces;


use Illuminate\Pagination\LengthAwarePaginator;

interface CanGetAllWithPaginate
{
    public function getAllWithPaginate(int $perPage): LengthAwarePaginator;
}
