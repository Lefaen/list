<?php


namespace App\Services\Interfaces;


use Illuminate\Database\Eloquent\Collection;

interface CanGetAllActive
{
    public function getAllActive(): Collection;
}
