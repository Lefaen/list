<?php


namespace App\Services\Interfaces;


interface iTaskListService extends
    CanCreate,
    CanUpdate,
    CanGetAll,
    CanGetOne,
    CanDelete,
    CanGetAllByUserId,
    CanGetAllWithPaginate
{

}
