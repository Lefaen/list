<?php


namespace App\Services\Interfaces;


use App\DataTransferObjects\DataTransferObject;
use App\DataTransferObjects\UserDto;
use Illuminate\Http\Request;

interface CanCreate
{
    public function create(DataTransferObject $dataTransferObject): void;
}
