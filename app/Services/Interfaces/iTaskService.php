<?php


namespace App\Services\Interfaces;


interface iTaskService
    extends
    CanCreate,
    CanUpdate,
    CanGetAll,
    CanGetOne,
    CanDelete,
    CanGetAllById,
    CanGetAllActive,
    CanGetAllWithPaginate,
    CanDeleteByListId
{

}
