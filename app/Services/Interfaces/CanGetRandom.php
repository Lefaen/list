<?php


namespace App\Services\Interfaces;


use Illuminate\Database\Eloquent\Model;

interface CanGetRandom
{
    public function getRandom(): Model;
}
