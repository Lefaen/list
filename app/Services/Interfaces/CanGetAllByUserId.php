<?php


namespace App\Services\Interfaces;


use Illuminate\Database\Eloquent\Collection;

interface CanGetAllByUserId
{
    public function getAllByUserId($id): Collection;
}
