<?php


namespace App\Services\Interfaces;


use App\DataTransferObjects\DataTransferObject;
use App\DataTransferObjects\UserDto;
use Illuminate\Http\Request;

interface CanUpdate
{
    public function update(int $id, DataTransferObject $dataTransferObject): void;
}
