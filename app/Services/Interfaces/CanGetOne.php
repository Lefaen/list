<?php


namespace App\Services\Interfaces;


use Illuminate\Database\Eloquent\Model;

interface CanGetOne
{
    public function getById(int $id): Model;
}
