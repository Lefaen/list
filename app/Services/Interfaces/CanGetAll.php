<?php


namespace App\Services\Interfaces;


use Illuminate\Database\Eloquent\Collection;

interface CanGetAll
{
    public function getAll(): Collection;
}
