<?php

namespace App\Http\Controllers\Web;


use App\DataTransferObjects\UserDto;
use App\Services\Interfaces\iUserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    private iUserService $userService;

    public function __construct(iUserService $userService)
    {
        $this->userService = $userService;
    }

    public function index()
    {
        $users = $this->userService->getAllWithPaginate();
        return view('users', ['users' => $users]);
    }

    public function one($id)
    {
        $user = $this->userService->getById($id);
        return view('user', ['user' => $user]);
    }

    public function delete($id)
    {
        $this->userService->deleteById($id);
    }

    public function create(Request $request, $id)
    {
        $userDto = UserDto::getFromRequest($request);
        $this->userService->create($userDto);
    }

    public function update(Request $request, $id)
    {
        $userDto = UserDto::getFromRequest($request);
        $this->userService->update($id, $userDto);
    }
}
