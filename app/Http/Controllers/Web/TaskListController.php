<?php

namespace App\Http\Controllers\Web;

use App\DataTransferObjects\TaskListDto;
use App\DataTransferObjects\UserDto;
use App\Services\Interfaces\iTaskService;
use App\Services\Interfaces\iTaskListService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TaskListController extends Controller
{
    private iTaskListService $todoListsService;
    private iTaskService $itemListsService;

    public function __construct(iTaskListService $todoListsService, iTaskService $itemListsService)
    {
        $this->todoListsService = $todoListsService;
        $this->itemListsService = $itemListsService;
    }

    public function index()
    {
        $lists = $this->todoListsService->getAllByUserId(Auth::id());

        return view('lists', ['lists' => $lists]);
    }

    public function one($id = null)
    {
        $list = null;
        if($id){
            $list = $this->todoListsService->getById($id);
        }

        return view('lists_one', ['list' => $list]);
    }

    public function create(Request $request)
    {
        $dto = TaskListDto::getFromRequest($request);
        $dto->setUserId(Auth::id());
        $this->todoListsService->create($dto);

        return redirect()->route('lists');
    }

    public function update(Request $request, $id)
    {
        $dto = TaskListDto::getFromRequest($request);
        $this->todoListsService->update($id, $dto);

        return redirect()->route('lists_one', ['id' => $id]);
    }

    public function delete($id)
    {
        $this->todoListsService->deleteById($id);
        $this->itemListsService->deleteByListId($id);

        return redirect()->route('lists');
    }
}
