<?php

namespace App\Http\Controllers\Web;

use App\DataTransferObjects\TaskDto;
use App\Repositories\TaskListRepository;
use App\Services\Interfaces\iTaskService;
use App\Services\Interfaces\iTaskListService;
use App\Services\TaskListService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    private iTaskService $service;
    private iTaskListService $todoListsService;

    public function __construct(iTaskService $service, iTaskListService $todoListsService)
    {
        $this->service = $service;
        $this->todoListsService = $todoListsService;
    }

    public function index($list_id)
    {
        $items = $this->service->getAllById($list_id);
        return view('items', [
            'list_id' => $list_id,
            'items' => $items
        ]);
    }

    public function one(Request $request, $list_id, $item_id = null)
    {
        $item = null;
        if($item_id){
            $item = $this->service->getById($item_id);
        }

        $lists = $this->todoListsService->getAllByUserId(Auth::id());

        return view('items_one', [
            'item' => $item,
            'lists' => $lists,
            'list_id' => $list_id,
        ]);
    }

    public function create(Request $request, $list_id)
    {
        $dto = TaskDto::getFromRequest($request);
        $this->service->create($dto);

        return redirect()->route('items', ['list_id' => $list_id]);
    }

    public function update(Request $request, $list_id, $item_id)
    {

        $dto = TaskDto::getFromRequest($request);
        $this->service->update($item_id, $dto);

        return redirect()->route('items_one', [
            'list_id' => $list_id,
            'item_id' => $item_id,
        ]);
    }

    public function close(Request $request, $list_id, $item_id)
    {

        $dto = TaskDto::getFromRequest($request);
        $this->service->update($item_id, $dto);

        return redirect()->route('home');
    }

    public function delete($list_id, $item_id)
    {
        $this->service->deleteById($item_id);

        return redirect()->route('items', [
            'list_id' => $list_id,
        ]);
    }
}
