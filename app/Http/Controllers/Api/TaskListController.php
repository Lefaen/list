<?php

namespace App\Http\Controllers\Api;

use App\DataTransferObjects\TaskDto;
use App\DataTransferObjects\TaskListDto;
use App\Models\TaskList;
use App\Repositories\TaskListRepository;
use App\Services\TaskListService;
use App\Services\TaskService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskListController extends Controller
{
    private TaskListService $taskListService;
    private TaskService $taskService;
    public function __construct(TaskListService $taskListService, TaskService $taskService)
    {
        $this->taskListService = $taskListService;
        $this->taskService = $taskService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = $this->taskListService->getAllWithPaginate(10);
        return response($lists, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dto = TaskListDto::getFromRequest($request);
        $this->taskListService->create($dto);

        return response(null, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $list = $this->taskListService->getById($id);

        return response($list, 200);
    }

    /**
     * CanUpdate the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dto = TaskListDto::getFromRequest($request);
        $this->taskListService->update($id, $dto);

        return response(null, 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->taskListService->deleteById($id);
        $this->taskService->deleteByListId($id);

        return response(null, 204);
    }
}
