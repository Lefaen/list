<?php

namespace App\Http\Controllers\Api;

use App\DataTransferObjects\UserDto;
use App\Models\User;
use App\Repositories\UsersRepository;
use App\Services\Interfaces\iUserService;
use App\Services\UsersService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    private iUserService $usersService;

    public function __construct(iUserService $usersService)
    {
        $this->usersService = $usersService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->usersService->getAllWithPaginate();

        return response($users, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userDto = UserDto::getFromRequest($request);
        $this->usersService->create($userDto);

        return response(null, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->usersService->getById($id);

        return response($user, 200);
    }

    /**
     * CanUpdate the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userDto = UserDto::getFromRequest($request);
        $this->usersService->update($id, $userDto);

        return response(null, 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->usersService->deleteById($id);

        return response(null, 204);
    }
}
