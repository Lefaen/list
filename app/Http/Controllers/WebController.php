<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\iTaskService;
use Illuminate\Http\Request;

class WebController extends Controller
{
    private iTaskService $service;
    public function __construct(iTaskService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $items = $this->service->getAllActive();
        return view('home', ['items' => $items]);
    }
}
