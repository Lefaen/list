<?php


namespace App\DataTransferObjects;


use Illuminate\Support\Str;

class UserDto extends DataTransferObject
{
    private string $name;
    private string $email;
    private string $password;
    private string $apiToken;

    public function __construct(array $data = [])
    {
        if(isset($data['name'])){
            $this->setName($data['name']);
        }
        if(isset($data['email'])){
            $this->setEmail($data['email']);
        }
        if(isset($data['password'])){
            $this->setPassword($data['password']);
        }
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function toArray(): array
    {
        $data = [];
        if (isset($this->name)) {
            $data['name'] = $this->name;
        }
        if (isset($this->email)) {
            $data['email'] = $this->email;
        }
        if (isset($this->password)) {
            $data['password'] = $this->password;
        }
        if (isset($this->apiToken)) {
            $data['api_token'] = $this->apiToken;
        }

        return $data;
    }

    public function generateToken()
    {
        $this->apiToken = Str::random(60);
    }
}
