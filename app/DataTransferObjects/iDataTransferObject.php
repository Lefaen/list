<?php


namespace App\DataTransferObjects;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface iDataTransferObject
{
    public static function getFromRequest(Request $request);
}
