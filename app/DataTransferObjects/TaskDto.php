<?php


namespace App\DataTransferObjects;


class TaskDto extends DataTransferObject
{
    private string $title;
    private string $description;
    private bool $isPublic;
    private int $listId;

    public function __construct(array $data = [])
    {
        if(isset($data['title'])){
            $this->setTitle($data['title']);
        }
        if(isset($data['description'])){
            $this->setDescription($data['description']);
        }
        if(isset($data['is_public'])){
            $this->setIsPublic($data['is_public']);
        }else{
            $this->isPublic = false;
        }
        if(isset($data['list_id'])){
            $this->setListId($data['list_id']);
        }
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setDescription($description = '')
    {
        $this->description = $description;
    }

    public function setIsPublic($isPublic)
    {
        if($isPublic == 'on' || is_bool($isPublic) && $isPublic == true){
            $this->isPublic = true;
        }elseif($isPublic == 'false'){
            $this->isPublic = false;
        }
    }

    public function setListId(int $id)
    {
        $this->listId = $id;
    }

    function toArray(): array
    {
        $data = [];
        if (isset($this->title)) {
            $data['title'] = $this->title;
        }
        if (isset($this->description)) {
            $data['description'] = $this->description;
        }
        if (isset($this->isPublic)) {
            $data['is_public'] = $this->isPublic;
        }
        if (isset($this->listId)) {
            $data['list_id'] = $this->listId;
        }

        return $data;
    }
}
