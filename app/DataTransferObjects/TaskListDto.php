<?php


namespace App\DataTransferObjects;


class TaskListDto extends DataTransferObject
{
    private string $title;
    private string $description;
    private int $userId;

    public function __construct(array $data = [])
    {
        if(isset($data['title'])){
            $this->setTitle($data['title']);
        }
        if(isset($data['description'])){
            $this->setDescription($data['description']);
        }
        if(isset($data['user_id'])){
            $this->setUserId($data['user_id']);
        }
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setDescription($description = '')
    {
        $this->description = $description;
    }

    public function setUserId($id)
    {
        $this->userId = (int)$id;
    }

    public function toArray(): array
    {
        $data = [];
        if (isset($this->title)) {
            $data['title'] = $this->title;
        }
        if (isset($this->description)) {
            $data['description'] = $this->description;
        }
        if (isset($this->userId)) {
            $data['user_id'] = $this->userId;
        }
        return $data;
    }


}
