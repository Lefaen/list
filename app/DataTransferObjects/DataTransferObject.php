<?php


namespace App\DataTransferObjects;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

abstract class DataTransferObject implements iDataTransferObject
{
    public static function getFromRequest(Request $request)
    {
        $request->exists(['_token', '_method']);
        $dto = new static($request->toArray());
        return $dto;
    }

    abstract function toArray(): array;
}
