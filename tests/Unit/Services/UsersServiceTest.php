<?php

namespace Services;

use App\DataTransferObjects\UserDto;
use App\Models\User;
use App\Repositories\UsersRepository;
use App\Services\Interfaces\iUserService;
use App\Services\UsersService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Tests\TestCase;

class UsersServiceTest extends TestCase
{
    use RefreshDatabase;

    private UsersService $usersService;

    protected function setUp(): void
    {
        parent::setUp();
        $userRepositoryMock = $this->createMock(UsersRepository::class);
        $this->usersService = new UsersService($userRepositoryMock);
    }

    public function testCreate()
    {
        $user = User::factory()->make();
        $data = $user->toArray();
        $data['password'] = $user->password;

        $userDto = $this->createMock(UserDto::class);
        $userDto->method('toArray')
            ->willReturn($data);

        $this->usersService->create($userDto);

        $this->assertDatabaseHas('users', $user->toArray());
    }

    public function testDeleteById()
    {
        $user = User::factory()->create();

        $this->usersService->deleteById($user->id);

        $this->assertDeleted('users', $user->toArray());
    }

    public function testUpdate()
    {
        $user = User::factory()->create();
        $newUser = User::factory()->make();

        $userDto = $this->createMock(UserDto::class);
        $userDto->method('toArray')
            ->willReturn($newUser->toArray());

        $this->usersService->update($user->id, $userDto);

        $user->refresh();

        foreach ($newUser->toArray() as $key => $value) {
            $this->assertEquals($user->$key, $value);
        }
    }
}
