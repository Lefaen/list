<?php

namespace Services;

use App\DataTransferObjects\TaskDto;
use App\Models\Task;
use App\Models\TaskList;
use App\Models\User;
use App\Repositories\TaskRepository;
use App\Repositories\UsersRepository;
use App\Services\Interfaces\iTaskService;
use App\Services\TaskService;
use App\Services\UsersService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Tests\TestCase;

class TaskServiceTest extends TestCase
{
    use RefreshDatabase;

    private iTaskService $service;

    protected function setUp(): void
    {
        parent::setUp();
        $taskRepositoryMock = $this->createMock(TaskRepository::class);
        $this->service = new TaskService($taskRepositoryMock);
        User::factory()->count(10)->create(0);
        TaskList::factory()->count(50)->create(0);
    }

    public function testCreate()
    {
        $item = Task::factory()->make();
        $data = $item->toArray();

        $dto = $this->createMock(TaskDto::class);
        $dto->method('toArray')
            ->willReturn($data);

        $this->service->create($dto);

        $this->assertDatabaseHas('task', $item->toArray());
    }

    public function testDeleteById()
    {
        $item = Task::factory()->create();

        $this->service->deleteById($item->id);

        $this->assertDeleted('task', $item->toArray());
    }

    public function testUpdate()
    {
        $item = Task::factory()->create();
        $newitem = Task::factory()->make();

        $dto = $this->createMock(TaskDto::class);
        $dto->method('toArray')
            ->willReturn($newitem->toArray());

        $this->service->update($item->id, $dto);

        $item->refresh();

        foreach ($newitem->toArray() as $key => $value) {
            $this->assertEquals($item->$key, $value);
        }
    }
}
