<?php

namespace Services;

use App\DataTransferObjects\TaskListDto;
use App\Models\TaskList;
use App\Models\User;
use App\Repositories\TaskListRepository;
use App\Services\Interfaces\iTaskListService;
use App\Services\TaskListService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Tests\TestCase;

class TaskListServiceTest extends TestCase
{
    use RefreshDatabase;

    private iTaskListService $service;

    protected function setUp(): void
    {
        parent::setUp();
        $taskListRepositoryMock = $this->createMock(TaskListRepository::class);
        $this->service = new TaskListService($taskListRepositoryMock);
        User::factory()->count(10)->create(0);
    }

    public function testCreate()
    {
        $user = TaskList::factory()->make();
        $data = $user->toArray();

        $dto = $this->createMock(TaskListDto::class);
        $dto->method('toArray')
            ->willReturn($data);

        $this->service->create($dto);

        $this->assertDatabaseHas('task_list', $user->toArray());
    }

    public function testDeleteById()
    {
        $user = TaskList::factory()->create();

        $this->service->deleteById($user->id);

        $this->assertDeleted('task_list', $user->toArray());
    }

    public function testUpdate()
    {
        $list = TaskList::factory()->create();
        $newlist = TaskList::factory()->make();

        $dto = $this->createMock(TaskListDto::class);
        $dto->method('toArray')
            ->willReturn($newlist->toArray());

        $this->service->update($list->id, $dto);

        $list->refresh();

        foreach ($newlist->toArray() as $key => $value) {
            $this->assertEquals($list->$key, $value);
        }
    }
}
