<?php

namespace Repositories;

use App\Models\TaskList;
use App\Models\User;
use App\Repositories\Interfaces\iTaskListRepository;
use App\Repositories\TaskListRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TaskListRepositoryTest extends TestCase
{
    use RefreshDatabase;

    private iTaskListRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();
        User::factory()->count(10)->create();
        $this->repository = new TaskListRepository();
    }

    public function testGetAllWithPaginate()
    {
        $num = 10;
        $perPage = 3;
        $lists = TaskList::factory()->count($num)->create();

        $getLists = $this->repository->getAllWithPaginate($perPage);

        $this->assertEquals($lists->count(), $getLists->total());
        $this->assertEquals($perPage, $getLists->count());
    }

    public function testGetById()
    {
        $list = TaskList::factory()->create();

        $getList = $this->repository->getById($list->id);

        $this->assertEquals($list->toArray(), $getList->toArray());
    }

    public function testGetAll()
    {
        $count = 10;
        TaskList::factory()->count($count)->create();

        $lists = $this->repository->getAll();

        $this->assertEquals($count, $lists->count());
    }
}
