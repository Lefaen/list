<?php

namespace Repositories;

use App\Models\Task;
use App\Models\TaskList;
use App\Models\User;
use App\Repositories\Interfaces\iTaskRepository;
use App\Repositories\Interfaces\iTaskListRepository;
use App\Repositories\TaskRepository;
use App\Repositories\TaskListRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TaskRepositoryTest extends TestCase
{
    use RefreshDatabase;

    private iTaskRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();
        User::factory()->count(10)->create();
        TaskList::factory()->count(50)->create();
        $this->repository = new TaskRepository();
    }

    public function testGetAllWithPaginate()
    {
        $num = 10;
        $perPage = 3;
        $lists = Task::factory()->count($num)->create();

        $getLists = $this->repository->getAllWithPaginate($perPage);

        $this->assertEquals($lists->count(), $getLists->total());
        $this->assertEquals($perPage, $getLists->count());
    }

    public function testGetById()
    {
        $list = Task::factory()->create();
        $list = Task::find($list->id);
        $getList = $this->repository->getById($list->id);

        $this->assertEquals($list->toArray(), $getList->toArray());
    }

    public function testGetAll()
    {
        $count = 10;
        Task::factory()->count($count)->create();

        $items = $this->repository->getAll();

        $this->assertEquals($count, $items->count());
    }

    public function testGetAllActive()
    {
        $count = 10;
        Task::factory()->count($count)->create([
            'is_public' => true
        ]);

        $items = $this->repository->getAllActive();

        $this->assertEquals($count, $items->count());
    }
}
