<?php

namespace Repositories;

use App\DataTransferObjects\UserDto;
use App\Models\User;
use App\Repositories\UsersRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UsersRepositoryTest extends TestCase
{
    use RefreshDatabase;
    private UsersRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new UsersRepository();
    }

    public function testGetAllWithPaginate()
    {
        $count = 10;
        $perPage = 3;
        User::factory()->count($count)->create();

        $users = $this->repository->getAllWithPaginate(3);

        $this->assertEquals($count, $users->total());
        $this->assertEquals($perPage, $users->count());
    }

    public function testGetAll()
    {
        $count = 10;
        User::factory()->count($count)->create();

        $users = $this->repository->getAll();

        $this->assertEquals($count, $users->count());
    }

    public function testGetById()
    {
        $user = User::factory()->create();

        $getUser = $this->repository->getById($user->id, [
            'id',
            'name',
            'email',
            'created_at',
            'updated_at',
        ]);

        $this->assertEquals($user->toArray(), $getUser->toArray());
    }

    public function testGetRandom()
    {
        User::factory()->count(10)->create();

        $user = $this->repository->getRandom();

        $this->assertDatabaseHas('users', $user->toArray());
    }
}
