<?php

namespace DataTransferObjects;

use App\DataTransferObjects\TaskListDto;
use PHPUnit\Framework\TestCase;

class TaskListDtoTest extends TestCase
{
    /**
     * @dataProvider taskListProvider
     */
    public function testToArray(array $data)
    {
        $taskListDto = new TaskListDto($data);

        $dataDto = $taskListDto->toArray();

        foreach ($data as $key => $value) {
            $this->assertEquals($value, $dataDto[$key]);
        }
    }

    public function taskListProvider()
    {
        return [
            [
                [
                    'title' => 'test',
                    'user_id' => 1,
                ]
            ]
        ];
    }
}
