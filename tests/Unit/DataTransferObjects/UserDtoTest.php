<?php

namespace DataTransferObjects;

use App\DataTransferObjects\UserDto;
use App\Models\User;
use Illuminate\Http\Request;
use Tests\TestCase;

class UserDtoTest extends TestCase
{

    /**
     * @dataProvider userProvider
     */
    public function testToArray(array $user)
    {
        $userDto = new UserDto($user);

        $dataDto = $userDto->toArray();

        $this->assertEquals($user, $dataDto);
    }

    /**
     * @dataProvider userProvider
     */
    public function testGenerateToken(array $user)
    {
        $userDto = new UserDto($user);

        $userDto->generateToken();

        $this->assertNotEmpty($user['api_token']);
    }

    public function userProvider()
    {
        return [
            [
                [
                    'name' => 'test_name',
                    'email' => 'test@test.test',
                    'password' => 'test_password',
                ]
            ]
        ];
    }
}
