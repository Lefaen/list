<?php

namespace DataTransferObjects;

use App\DataTransferObjects\TaskDto;
use App\Models\Task;
use Tests\TestCase;

class TaskDtoTest extends TestCase
{

    /**
     * @dataProvider taskProvider
     */
    public function testToArray(array $data)
    {
        $taskDto = new TaskDto($data);

        $dataDto = $taskDto->toArray();

        foreach ($data as $key => $value) {
            $this->assertEquals($value, $dataDto[$key]);
        }
    }

    public function taskProvider()
    {
        return [
            [
                [
                    'title' => 'test',
                    'list_id' => 1,
                ]
            ]
        ];
    }
}
