<?php

namespace Tests\Feature;

use App\Models\TaskList;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TaskListControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    private string $token;
    protected function setUp(): void
    {
        parent::setUp();
        $users = User::factory()->count(10)->create();
        $this->token = $users->random()->createToken('test')->plainTextToken;
    }

    public function testIndex()
    {
        $lists = TaskList::factory()->count(100)->create();

        $response = $this->json('GET', 'api/lists', [], [
            'Authorization' => 'Bearer ' . $this->token,
        ]);
        $response->assertStatus(200)->assertJsonStructure([
            'data' => [
                '*' => [
                    'title',
                    'description',
                    'user_id',
                    'created_at',
                    'updated_at',
                ]
            ],
            'per_page',
            'current_page',
            'total',
        ]);

        $users = TaskList::orderByDesc('id')
            ->limit(10)
            ->get()
            ->toArray();

        $receivedLists = $response->json('data');
        $this->assertCount(10, $receivedLists);

        $this->assertEquals($receivedLists, $users);
    }

    public function testShow()
    {
        $list = TaskList::factory()->create();

        $response = $this->json('GET', 'api/lists/' . $list->id, [], [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        $response->assertStatus(200)->assertJsonStructure([
            'id',
            'description',
            'title',
            'user_id',
            'created_at',
            'updated_at',
        ]);

        $this->assertEquals($list->toArray(), $response->json());
    }

    public function testStore()
    {
        $list = TaskList::factory()->make();
        $response = $this->json('POST', 'api/lists', $list->toArray(), [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        $response->assertStatus(201);
        $this->assertDatabaseHas('task_list', $list->toArray());
    }

    public function testUpdate()
    {
        $list = TaskList::factory()->create();
        $newList = TaskList::factory()->make();

        $response = $this->json('PUT', 'api/lists/' . $list->id, $newList->toArray(), [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        $response->assertStatus(204);

        $list->refresh();
        foreach ($newList->toArray() as $key => $value){
            $this->assertEquals($list->$key, $value);
        }
    }

    public function testDestroy()
    {
        $list = TaskList::factory()->create();

        $response = $this->json('DELETE', 'api/lists/' . $list->id, [], [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        $response->assertStatus(204);

        $this->assertDeleted($list);

    }
}
