<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\TaskList;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TaskControllerTest extends TestCase
{
    use RefreshDatabase;

    private string $token;

    protected function setUp(): void
    {
        parent::setUp();
        $users = User::factory()->count(10)->create();
        $lists = TaskList::factory()->count(100)->create();
        $this->token = $users->random()->createToken('test')->plainTextToken;
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIndex()
    {
        Task::factory()->count(500)->create();
        $items = Task::orderByDesc('id')
            ->limit(10)
            ->get()
            ->toArray();


        $response = $this->json('GET', 'api/items', [], [
            'Authorization' => 'Bearer ' . $this->token,
        ]);
        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'title',
                    'list_id',
                    'created_at',
                    'updated_at',
                ]
            ],
            'per_page',
            'current_page',
            'total',
        ]);

        $this->assertCount(10, $response->json('data'));
        $this->assertEquals($response->json('data'), $items);
    }

    public function testShow()
    {
        $item = Task::factory()->create();
        $response = $this->json('GET', 'api/items/' . $item->id, [], [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'id',
            'title',
            'list_id',
            'created_at',
            'updated_at',
        ]);
    }

    public function testStore()
    {
        $item = Task::factory()->make();
        $response = $this->json('POST', 'api/items', $item->toArray(), [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        $response->assertStatus(201);
        $this->assertDatabaseHas('task', $item->toArray());
    }

    public function testUpdate()
    {
        $item = Task::factory()->create();
        $newItem = Task::factory()->make();

        $response = $this->json('PUT', 'api/items/' . $item->id, $newItem->toArray(), [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        $response->assertStatus(204);

        $item->refresh();
        foreach ($newItem->toArray() as $key => $value) {
            $this->assertEquals($item->$key, $value);
        }
    }

    public function testDestroy()
    {
        $item = Task::factory()->create();
        $response = $this->json('DELETE', 'api/items/' . $item->id, [], [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        $response->assertStatus(204);
        $this->assertDeleted($item);
    }
}
