<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [Api\AuthController::class, 'login']);

Route::group(['middleware' => 'auth:sanctum'], function (){
    Route::apiResource('users', Api\UsersController::class);
    Route::apiResource('lists', Api\TaskListController::class);
    Route::apiResource('items', Api\TaskController::class);
});
