<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function (){
    Route::get('/', [\App\Http\Controllers\WebController::class, 'index'])->name('home');

    Route::get('/lists', [Web\TaskListController::class, 'index'])->name('lists');
    Route::post('/lists', [Web\TaskListController::class, 'create'])->name('lists_create');

    Route::get('/lists/new', [Web\TaskListController::class, 'one'])->name('lists_new');
    Route::get('/lists/{id}', [Web\TaskListController::class, 'one'])->name('lists_one');
    Route::put('/lists/{id}', [Web\TaskListController::class, 'update'])->name('lists_update');
    Route::delete('/lists/{id}', [Web\TaskListController::class, 'delete'])->name('lists_delete');


    Route::get('/lists/{list_id}/items', [Web\TaskController::class, 'index'])->name('items');
    Route::post('/lists/{list_id}/items', [Web\TaskController::class, 'create'])->name('items_create');

    Route::get('/lists/{list_id}/items/new', [Web\TaskController::class, 'one'])->name('items_new');
    Route::get('/lists/{list_id}/items/{item_id}', [Web\TaskController::class, 'one'])->name('items_one');
    Route::put('/lists/{list_id}/items/{item_id}', [Web\TaskController::class, 'update'])->name('items_update');
    Route::put('/lists/{list_id}/items/{item_id}/close', [Web\TaskController::class, 'close'])->name('items_close');
    Route::delete('/lists/{list_id}/items/{item_id}', [Web\TaskController::class, 'delete'])->name('items_delete');
});


