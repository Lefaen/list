@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                            <a href="{{ route('lists_new', [], false) }}">{{ __('lists.create_list') }}</a>
                            <table>
                                <tr>
                                    <th>{{ __('common.title') }}</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                        @foreach($lists as $list)
                            <tr>
                                <td>
                                    <div>
                                        <a href="{{ route('lists_one', [$list->id], false) }}/items">
                                            {{ $list->title }}
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <a href="{{ route('lists_update', [$list->id], false) }}">{{ __('common.update') }}</a>
                                </td>
                                <td>
                                    <form action="{{ route('lists_delete',  [$list->id], false) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="submit">{{ __('common.delete') }}</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
