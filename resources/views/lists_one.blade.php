@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>
                    <div class="card-body">
                        <a href="{{ route('lists', [], false) }}">{{ __('lists.lists') }}</a>
                        <form action="{{ ($list)? route('lists_update', [$list->id], false):route('lists_create', [], false) }}" method="post">
                            @if($list)
                                @method('put')
                            @endif
                            @csrf
                            @if($list)
                                <div>
                                    <span>{{ __('common.created_at') }}:</span>
                                    <span>{{ $list->created_at }}</span>
                                </div>
                                <div>
                                    <span>{{ __('common.updated_at') }}:</span>
                                    <span>{{ $list->updated_at }}</span>
                                </div>
                            @endif
                            <label>
                                <span>{{ __('common.title') }}</span>
                                <input type="text" name="title" value="{{ ($list->title)??'' }}">
                            </label>
                            <br>
                            <label>
                                <span>{{ __('common.description') }}</span>
                                <textarea name="description">{{ ($list->description)??'' }}</textarea>
                            </label>
                            <br>
                            @if($list)
                                <button type="submit">{{ __('common.update') }}</button>
                            @else
                                <button type="submit">{{ __('common.create') }}</button>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
