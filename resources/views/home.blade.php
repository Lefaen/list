@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <ul>
                        <li>
                            <a href="{{ route('lists', [], false) }}">{{ __('lists.lists') }}</a>
                        </li>
                    </ul>
                    <div>
                        <span>{{ __('items.active_tasks') }}:</span>
                        <ul>
                            @foreach($items as $item)
                            <li>
                                <form action="{{ route('items_close', [$item->list_id, $item->id], false) }}" method="post">
                                    @method('put')
                                    @csrf
                                    {{ $item->title }}
                                    <input type="hidden" name="is_public" value="">
                                    <button type="submit">{{ __('common.close') }}</button>
                                </form>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
