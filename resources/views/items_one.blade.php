@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>
                    <div class="card-body">
                        <a href="{{ route('items', [$list_id], false) }}">{{ __('items.list') }}</a>
                        <form action="{{ ($item)? route('items_update', [$item->list_id, $item->id], false):route('items_create', [$list_id], false) }}" method="post">
                            @if($item)
                                @method('put')
                            @endif
                            @csrf
                            <table>
                                <tr>
                                    <th>{{ __('items.property') }}</th>
                                    <th>{{ __('items.value') }}</th>
                                </tr>
                                <tr>
                                    <td>{{ __('items.public') }}:</td>
                                    <td>
                                        <input type="checkbox"
                                               name="is_public" {{ ((isset($item->is_public) && $item->is_public == true) || !isset($item->is_public))?'checked':'' }}>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{ __('common.title') }}:</td>
                                    <td>
                                        <input type="text" name="title" value="{{ ($item->title)??'' }}">
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{ __('common.description') }}:</td>
                                    <td>
                                        <textarea name="description">{{ ($item->description)??'' }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{ __('items.list') }}:</td>
                                    <td>
                                        <select name="list_id" id="">
                                            @foreach($lists as $list)
                                                <option
                                                    {{ (($item) && $list->id == $item->list_id)?'selected':'' }}
                                                    value="{{ $list->id }}">
                                                    {{ $list->title }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                @if($item)
                                <tr>
                                    <td>{{ __('common.created_at') }}:</td>
                                    <td>
                                        {{ ($item->created_at)??'' }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{ __('common.updated_at') }}:</td>
                                    <td>
                                        {{ ($item->updated_at)??'' }}
                                    </td>
                                </tr>
                                @endif
                            </table>
                                @if($item)
                                    <button type="submit">{{ __('common.update') }}</button>
                                @else
                                    <button type="submit">{{ __('common.create') }}</button>
                                @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
