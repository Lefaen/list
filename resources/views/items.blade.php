@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>
                    <div class="card-body">
                            <a href="{{ route('items_new', [$list_id], false) }}">{{ __('items.create_task') }}</a>
                        <div>
                            <table>
                                <tr>
                                    <th>{{ __('items.active') }}</th>
                                    <th>{{ __('common.title') }}</th>
                                    <th></th>
                                </tr>
                            @foreach($items as $item)
                                <tr>
                                    <td>
                                        <input type="checkbox" {{ ($item->is_public)?'checked':'' }} onclick="return false">
                                    </td>
                                    <td>
                                        <a href="{{ route('items_one', [$item->list_id, $item->id], false) }}">
                                            {{ $item->title }}
                                        </a>
                                    </td>
                                    <td>
                                        <form action="{{ route('items_delete', [$item->list_id, $item->id], false) }}" method="post">
                                            @method('delete')
                                            @csrf
                                            <button type="submit">{{ __('common.delete') }}</button>
                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
