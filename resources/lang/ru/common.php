<?php

return [
    'create_list' => 'Создать список',
    'title' => 'Название',
    'property' => 'Свойство',
    'value' => 'Значение',
    'description' => 'Описание',
    'created_at' => 'Создано',
    'updated_at' => 'Обновлено',
    'delete' => 'Удалить',
    'update' => 'Изменить',
    'create' => 'Создать',
    'close' => 'Закрыть',
];
