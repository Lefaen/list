<?php

return [
    'create_task' => 'Создать задачу',
    'active' => 'Активность',
    'property' => 'Свойство',
    'value' => 'Значение',
    'public' => 'Опубликовано',
    'list' => 'Список',
    'active_tasks' => 'Активные задачи',
];
